//**************************************************************************
//
// Date
//
// Product
//
// Or Module
//
// Copyright        PCME PROPRIETARY AND CONFIDENTIAL
//                  SOFTWARE FILE/MODULE HEADER
//                  PCME (c) 2016
//                  Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
//                  All Rights Reserved
//**************************************************************************

/* ----------------------- System includes --------------------------------*/
/* ----------------------- Application includes ---------------------------*/
#include "rollingAverage.h"


/* ----------------------- Defines ----------------------------------------*/
/* ----------------------- Type Definitions -------------------------------*/
/* ----------------------- Local  variables -------------------------------*/
/* ----------------------- Local  Prototypes ------------------------------*/
/* ----------------------- Global Functions -------------------------------*/
void raAppendNewValue(raDriver_t* driver, float val)
{
    //append new value and calculate new average
    driver->avg = driver->avg + (val - driver->avg)/driver->itr;
    
    //tidy up iterator
    driver->itr++;
    if(driver->itr > driver->maxItr)
        driver->itr = driver->maxItr;
}


void raResetDriver(raDriver_t* driver, uint32_t maxItr)
{
    if(maxItr < 1) maxItr = 1;
    
    driver->avg = 0;
    driver->itr = 1;
    driver->maxItr = maxItr;
}


float raGetAvgValue(raDriver_t* driver)
{
    return driver->avg;
}


/* ----------------------- Local  Functions -------------------------------*/
