/**
  ******************************************************************************
  * @file	 timeoutTimers.h
  * @author  Michal Kalbarczyk
  * @version
  * @date	 18 Jun 2015
  * @brief
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef TIMEOUTTIMERS_H_
#define TIMEOUTTIMERS_H_


/* Includes ------------------------------------------------------------------*/
#include <systemDefinitions.h>


/* Exported constants --------------------------------------------------------*/
/* Exported define -----------------------------------------------------------*/
#define UART_WAIT_FOR_NEW_CHAR_TIMEOUT	10

#define NUMBER_OF_TIMEOUT_TIMERS 		3


/* Exported types ------------------------------------------------------------*/
typedef enum{
	TT_AUX_USART_WAIT_FOR_NEW_CHAR 		= 0,

	TT_LED_RED_TIMER					= 1,
	TT_LED_GREEN_TIMER					= 2
}timersNames;


typedef struct{
	bool enabled;
	bool elapsed;
	uint32_t period;
	uint32_t counter;
}timeoutTimerStc;


/* Exported macro ------------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/
void initTimeoutTimers(void);
void deInitTimeoutTimers(void);
void setTimeoutTimer(timersNames, uint32_t);
bool hasTimeoutTimerElapsed(timersNames);
void resetTimeoutTimer(timersNames);


/* Exported interrupt functions ----------------------------------------------*/
void updateTimeoutTimers(void);


/* Exported variables --------------------------------------------------------*/
extern TIM_HandleTypeDef htim3;


#endif /* TIMEOUTTIMERS_H_ */
