/*
 * tests.c
 *
 *  Created on: 11 Dec 2017
 *      Author: michalk
 */

#include "rmsAlgorithm.h"
#include "firFilter.h"

#include "timestamps.h"
#include "sampleData.h"

#include "acEngine.h"
#include "acCommands.h"


#define RMS_TEST_SAMPLES			SD_NBR_OF_TEST_SAMPLES
#define RMS_TEST_WINDOW				2500
#define RMS_NBR_OF_WINDOWS			(RMS_TEST_SAMPLES/RMS_TEST_WINDOW)


static float firCoefs[FIR_NBR_OF_COEFS] = {
		 0.001533491022156642, -0.001888311544501797,  0.001713449997280088, -0.0001583074464945825,
		-0.003444993345788122,  0.008445884576026748, -0.01220721839146596,   0.0107046978629928,
		-0.0005492194527706845,-0.01831467822231439,   0.0405865063829215,   -0.05543661403965807,
		 0.04815927677380113,  -0.0009188852171992643,-0.1211665881917967,    0.6029415092368108,
		 0.6029415092368108,   -0.1211665881917967,   -0.0009188852171992646, 0.04815927677380113,
		-0.05543661403965807,   0.04058650638292149,  -0.0183146782223144,   -0.0005492194527706849,
		 0.0107046978629928,   -0.01220721839146596,   0.008445884576026748, -0.003444993345788122,
		-0.0001583074464945825, 0.001713449997280087, -0.001888311544501799,  0.001533491022156641
};

static wrmsDataParams_t wrmsDataParams;
static firDataParams_t  firDataParams;



void testRMS_1(void)
{
	uint32_t i;
	const float* dataPtr;
	float rmsResult;

	rmsDriver_t rmsDriver;
	rmsConfig_t rmsConfig;


	//prepare RMS driver
	rmsPreSetDriver(&rmsDriver);

	rmsConfig.nbrOfSamplesPerWindow = RMS_TEST_WINDOW;
	rmsConfig.nbrOfWindows 			= 5; //RA_NBR_OF_WINDOWS;
	rmsConfigureDriver(&rmsDriver, &rmsConfig);

	//startCollectingTimestamps(1);

	collectTimestamp(10);

	//run samples processing algorithm
	for(i=0; i<RMS_NBR_OF_WINDOWS; i++)
	{
		collectTimestamp(20);
		dataPtr = &sampleFilteredData[i*RMS_TEST_WINDOW];
		rmsAddNewSamplesMatrix(&rmsDriver, dataPtr);
		collectTimestamp(21);
	}

	collectTimestamp(11);

	rmsResult = rmsGetRMSValue(&rmsDriver);

	asm("nop");

	rmsReSetDriver(&rmsDriver);

	(void)(rmsResult);
}



void testRMS_2(void)
{
	uint32_t i;
	const float* dataPtr;
	float rmsResult;

	rmsDriver_t rmsDriver;
	rmsConfig_t rmsConfig;


	//prepare RMS driver
	rmsPreSetDriver(&rmsDriver);

	rmsConfig.nbrOfSamplesPerWindow = 5; //RA_TEST_WINDOW;
	rmsConfig.nbrOfWindows 			= 5; //RA_NBR_OF_WINDOWS;
	rmsConfigureDriver(&rmsDriver, &rmsConfig);

	startCollectingTimestamps(1);

	collectTimestamp(30);

	//run samples processing algorithm
	dataPtr = &sampleFilteredData[0];
	for(i=0; i<RMS_TEST_SAMPLES; i++)
	{
		collectTimestamp(40);
		rmsAddNewSample(&rmsDriver, *dataPtr);
		collectTimestamp(41);

		dataPtr++;
	}

	collectTimestamp(31);

	rmsResult = rmsGetRMSValue(&rmsDriver);

	asm("nop");

	rmsReSetDriver(&rmsDriver);

	(void)(rmsResult);
}


/**
 * @brief Power test.
 */
void testRMS_3(void)
{
	float rmsResult;

	rmsDriver_t rmsDriver;
	rmsConfig_t rmsConfig;


	//prepare RMS driver
	rmsPreSetDriver(&rmsDriver);

	rmsConfig.nbrOfSamplesPerWindow = RMS_TEST_WINDOW;
	rmsConfig.nbrOfWindows 			= RMS_NBR_OF_WINDOWS;
	rmsConfigureDriver(&rmsDriver, &rmsConfig);

	//startCollectingTimestamps(1);

	collectTimestamp(10);

	//run samples processing algorithm
	while(1)
	{
		collectTimestamp(20);
		rmsAddNewSamplesMatrix(&rmsDriver, &sampleFilteredData[0]);
		collectTimestamp(21);
	}

	collectTimestamp(11);

	rmsResult = rmsGetRMSValue(&rmsDriver);

	asm("nop");

	rmsReSetDriver(&rmsDriver);

	(void)(rmsResult);
}






void testFIR_1(void)
{

}


void testFIR_2(void)
{
	uint32_t i;
	volatile float firResult;

	firDriver_t firDriver;
	firConfig_t firConfig;


	firPreSetDriver(&firDriver);

	firConfig.firCoefs 		= &firCoefs[0];
	firConfig.nbrOfCoefs 	= FIR_NBR_OF_COEFS;

	firConfigureDriver(&firDriver, &firConfig);

	startCollectingTimestamps(1);

	collectTimestamp(10);

	for(i=0; i<SD_NBR_OF_TEST_SAMPLES; i++)
	{
		collectTimestamp(20);
		firAddNewSample(&firDriver, sampleRawData[i]);
		collectTimestamp(21);
	}

	collectTimestamp(11);

	firResult = firGetFIRValue(&firDriver);

	asm("nop");

	firReSetDriver(&firDriver);

	(void)(firResult);
}



void startWrsmUartTest(void)
{
	uint32_t i;
	bool confRes;

	TxQueueItem_t item;

	rmsDriver_t rmsDriver;
	rmsConfig_t rmsConfig;


	//prepare RMS driver
	rmsPreSetDriver(&rmsDriver);

	rmsConfig.nbrOfSamplesPerWindow = 1;
	rmsConfig.nbrOfWindows 			= 2500;
	confRes = rmsConfigureDriver(&rmsDriver, &rmsConfig);

	if(false == confRes)
	{
		addShortMessageToQueue(COMM_FINISHED_WRMS_CALCULATIONS);
		return;
	}

	//prepare AUX com message beforehand
	item.paramPtr  = (unsigned char*)&wrmsDataParams;
	item.paramSize = sizeof(wrmsDataParams_t);
	item.command   = COMM_WRMS_DATA;

	//run through the filter algorithm and send data
	for(i=0; i<SD_NBR_OF_TEST_SAMPLES; i++)
	{
		rmsAddNewSample(&rmsDriver, sampleFilteredData[i]);
		wrmsDataParams.data = rmsGetRMSValue(&rmsDriver);

		//make sure that message gets to the queue
		while(false == addMessageToQueueBlockingMode(&item)){}
	}

	rmsReSetDriver(&rmsDriver);

	while(false == addShortMessageToQueue(COMM_FINISHED_WRMS_CALCULATIONS))
		asm("nop");
}


void startWrsmOptimisedUartTest(void)
{
	uint32_t i;
	bool confRes;

	TxQueueItem_t item;

	rmsDriver_t rmsDriver;
	rmsConfig_t rmsConfig;


	//prepare RMS driver
	rmsPreSetDriver(&rmsDriver);

	rmsConfig.nbrOfSamplesPerWindow = 1;
	rmsConfig.nbrOfWindows 			= 2500;
	confRes = rmsConfigureDriver(&rmsDriver, &rmsConfig);

	if(false == confRes)
	{
		addShortMessageToQueue(COMM_FINISHED_WRMS_CALCULATIONS);
		return;
	}

	//prepare AUX com message beforehand
	item.paramPtr  = (unsigned char*)&wrmsDataParams;
	item.paramSize = sizeof(wrmsDataParams_t);
	item.command   = COMM_WRMS_DATA;

	//run through the filter algorithm and send data
	for(i=0; i<SD_NBR_OF_TEST_SAMPLES; i++)
	{
		rmsAddNewSampleOptimised(&rmsDriver, sampleFilteredData[i]);
		wrmsDataParams.data = rmsGetRMSValue(&rmsDriver);

		//make sure that message gets to the queue
		while(false == addMessageToQueueBlockingMode(&item)){}
	}

	rmsReSetDriver(&rmsDriver);

	while(false == addShortMessageToQueue(COMM_FINISHED_WRMS_CALCULATIONS))
		asm("nop");
}


void startFakeWrsmUartTest(void)
{
	uint32_t i;
	bool confRes;

	TxQueueItem_t item;

	fakeRmsDriver_t rmsDriver;
	fakeRmsConfig_t rmsConfig;


	//prepare RMS driver
	fakeRmsPreSetDriver(&rmsDriver);

	rmsConfig.nbrOfSamples = 2500;
	confRes = fakeRmsConfigureDriver(&rmsDriver, &rmsConfig);

	if(false == confRes)
	{
		addShortMessageToQueue(COMM_FINISHED_WRMS_CALCULATIONS);
		return;
	}

	//prepare AUX com message beforehand
	item.paramPtr  = (unsigned char*)&wrmsDataParams;
	item.paramSize = sizeof(wrmsDataParams_t);
	item.command   = COMM_WRMS_DATA;

	//run through the filter algorithm and send data
	for(i=0; i<SD_NBR_OF_TEST_SAMPLES; i++)
	{
		fakeRmsAddNewSample(&rmsDriver, sampleFilteredData[i]);
		wrmsDataParams.data = fakeRmsGetRMSValue(&rmsDriver);

		//make sure that message gets to the queue
		while(false == addMessageToQueueBlockingMode(&item)){}
	}

	fakeRmsReSetDriver(&rmsDriver);

	while(false == addShortMessageToQueue(COMM_FINISHED_WRMS_CALCULATIONS))
		asm("nop");
}



/**
 * @warning This test is blocking reception of any incoming messages!!!
 */
void startFirUartTest(void)
{
	uint32_t i;
	bool confRes;

	TxQueueItem_t item;

	firDriver_t firDriver;
	firConfig_t firConfig;


	firPreSetDriver(&firDriver);

	firConfig.firCoefs 		= &firCoefs[0];
	firConfig.nbrOfCoefs 	= FIR_NBR_OF_COEFS;

	confRes = firConfigureDriver(&firDriver, &firConfig);

	if(false == confRes)
	{
		addShortMessageToQueue(COMM_FINISHED_FIR_CALCULATIONS);
		return;
	}

	//prepare AUX com message beforehand
	item.paramPtr  = (unsigned char*)&firDataParams;
	item.paramSize = sizeof(firDataParams_t);
	item.command   = COMM_FIR_DATA;

	//run through the filter algorithm and send data
	for(i=0; i<SD_NBR_OF_TEST_SAMPLES; i++)
	{
		//firAddNewSample(&firDriver, sampleRawData[i]);
		firAddNewSample(&firDriver, sinetoneRawData[i]);
		firDataParams.data = firGetFIRValue(&firDriver);

		//make sure that message gets to the queue
		while(false == addMessageToQueueBlockingMode(&item)){}

	}

	firReSetDriver(&firDriver);

	while(false == addShortMessageToQueue(COMM_FINISHED_FIR_CALCULATIONS))
		asm("nop");
}



