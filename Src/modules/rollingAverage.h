//**************************************************************************
//
// Date             
//
// Product
//
// Or Module
//
// Copyright        PCME PROPRIETARY AND CONFIDENTIAL
//                  SOFTWARE FILE/MODULE HEADER
//                  PCME (c) 2016
//                  Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
//                  All Rights Reserved
//**************************************************************************

#ifndef _ROLLING_AVERAGE_H_
#define _ROLLING_AVERAGE_H_


/* ----------------------- System includes --------------------------------*/
#include "systemDefinitions.h"


/* ----------------------- Application includes ---------------------------*/
/* ----------------------- Defines ----------------------------------------*/
/* ----------------------- Type Definitions -------------------------------*/

typedef struct{    
	float avg;
    uint32_t itr;
    uint32_t maxItr;
}raDriver_t;


/* ----------------------- Global variables -------------------------------*/
/* ----------------------- Global Prototypes ------------------------------*/
void raAppendNewValue(raDriver_t* driver, float val);
void raResetDriver(raDriver_t* driver, uint32_t maxItr);
float raGetAvgValue(raDriver_t* driver);


#endif  /*_ROLLING_AVERAGE_H_*/
