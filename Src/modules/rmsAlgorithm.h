//**************************************************************************
//
// Date				06.12.2017
//
// Product			Electrodynamic sensor
//
// Or Module
//
// Copyright        PCME PROPRIETARY AND CONFIDENTIAL
//                  SOFTWARE FILE/MODULE HEADER
//                  PCME (c) 2016
//                  Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
//                  All Rights Reserved
//**************************************************************************


#ifndef _RMS_ALGORITHM_H_
#define _RMS_ALGORITHM_H_

/* ----------------------- System includes --------------------------------*/
/* ----------------------- Application includes ---------------------------*/
#include <systemDefinitions.h>

#include "rollingAverage.h"


/* ----------------------- Defines ----------------------------------------*/
/* ----------------------- Type Definitions -------------------------------*/

typedef struct
{
	uint32_t nbrOfSamplesPerWindow;
	uint32_t nbrOfWindows;
}rmsConfig_t;


typedef struct
{
	float value;		/*!< summed values*/
	uint32_t ctr;		/*!< counter of added samples*/
}rmsSqWindow_t;


typedef struct
{
	rmsConfig_t config;
	
	rmsSqWindow_t* sqWindows;
	uint32_t activeWindow;
	uint32_t totalSampCtr;		/*!< total number of collected values in all blocks*/
	uint32_t maxSampCtr;		/*!< maximum number of samples needed for RMS calculation*/
	
	float squareSums;
	float rmsValue;
}rmsDriver_t;






typedef struct
{
	uint32_t nbrOfSamples;
}fakeRmsConfig_t;


typedef struct
{
	fakeRmsConfig_t config;
	raDriver_t 		rollAvg;
	float 			rmsValue;
}fakeRmsDriver_t;


/* ----------------------- Global variables -------------------------------*/
/* ----------------------- Global Prototypes ------------------------------*/
void  rmsPreSetDriver(rmsDriver_t* driver);
void  rmsReSetDriver(rmsDriver_t* driver);
bool  rmsConfigureDriver(rmsDriver_t* driver, rmsConfig_t* newConfig);
void  rmsAddNewSample(rmsDriver_t* driver, float sample);
void  rmsAddNewSampleOptimised(rmsDriver_t* driver, float sample);
void  rmsAddNewSamplesMatrix(rmsDriver_t* driver, const float* samples);
float rmsGetRMSValue(rmsDriver_t* driver);

void  fakeRmsPreSetDriver(fakeRmsDriver_t* driver);
void  fakeRmsReSetDriver(fakeRmsDriver_t* driver);
bool  fakeRmsConfigureDriver(fakeRmsDriver_t* driver, fakeRmsConfig_t* newConfig);
void  fakeRmsAddNewSample(fakeRmsDriver_t* driver, float sample);
float fakeRmsGetRMSValue(fakeRmsDriver_t* driver);


#endif   /* _RMS_ALGORITHM_H_ */
















