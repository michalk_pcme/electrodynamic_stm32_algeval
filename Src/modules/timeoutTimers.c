/**
  ******************************************************************************
  * @file    timeoutTimers.c
  * @author  Michal Kalbarczyk
  * @version 0.1
  * @date    18-June-2015
  * @brief
  * @details	HOW TO USE:
  * 			1. Add new timersNames enum and increase NUMBER_OF_TIMEOUT_TIMERS in timeoutTimers.h
  * 			2. Set and start timer with setTimeoutTimer() function
  * 			3. Check for elapsed time flag with hasTimeoutTimerElapsed() function
  *
  * @note Used peripherals:
  * @warning
  */


/* Includes ------------------------------------------------------------------*/
#include "timeoutTimers.h"
#include "stm32l4xx_hal.h"
#include <systemDefinitions.h>


/* Public variables ----------------------------------------------------------*/
TIM_HandleTypeDef htim3;


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static volatile timeoutTimerStc timeoutTimers[NUMBER_OF_TIMEOUT_TIMERS];


/* Private function prototypes -----------------------------------------------*/
static void configureHardwareTimer(void);


/* Public functions definitions ----------------------------------------------*/

/**
  * @brief  Initialises timeout timers module (hardware and software).
  * @note	Should be called at the very beginning during system initialization.
  * @param  None
  * @retval None
  */
void initTimeoutTimers(void){

	int i;
	volatile timeoutTimerStc * timer = &timeoutTimers[0];

	configureHardwareTimer();

	for(i=0;i<NUMBER_OF_TIMEOUT_TIMERS;i++){

		timer->enabled = false;
		timer->elapsed = false;
		timer->counter = 0;
		timer->period = 0;

		timer++;
	}

	HAL_TIM_Base_Start_IT(&htim3);
}


/**
  * @brief  De-initializes timeout timers hardware.
  * @param  None
  * @retval None
  */
void deInitTimeoutTimers(void){

	disableInterrupts();

	HAL_TIM_Base_DeInit(&htim3);

	enableInterrupts();
}


/**
  * @brief  Sets up specified timer for count-down and starts it.
  * @param  name:   timeout timer name
  * @param  period: time in [ms] after which timer will elapse
  * @retval None
  */
void setTimeoutTimer(timersNames name, uint32_t period){

	volatile timeoutTimerStc * timer = &timeoutTimers[name];

	//enter critical section
	DIS_INT

	if(0 == period){
		//special case to just make timer to elapse immediately
		timer->elapsed = true;
		timer->enabled = false;
		timer->counter = 0;
		timer->period  = 0;
	}
	else{
		timer->enabled = true;
		timer->elapsed = false;
		timer->counter = 0;
		timer->period = period;
	}

	//exit critical section
	EN_INT
}


/**
  * @brief   Checks if specified timer has elapsed.
  * @param   name:   timeout timer name
  * @retval  B_TRUE - timer has elapsed
  */
bool hasTimeoutTimerElapsed(timersNames name){

	return timeoutTimers[name].elapsed;
}


/**
  * @brief   Resets specified timer to default values.
  * @param   name:   timeout timer name
  * @retval  None
  */
void resetTimeoutTimer(timersNames name){

	volatile timeoutTimerStc * timer = &timeoutTimers[name];

	//enter critical section
	DIS_INT

	timer->enabled = false;
	timer->elapsed = false;
	timer->counter = 0;
	timer->period = 0;

	//exit critical section
	EN_INT

}


/* Interrupt functions definitions -------------------------------------------*/

/**
  * @brief   Interrupt function. Updates enabled timers counters. Triggered every 1ms.
  * @note    Should be placed in related hardware timer interrupt function.
  * @param   None
  * @retval  None
  */
void updateTimeoutTimers(void){

	int i;
	volatile timeoutTimerStc * timer = &timeoutTimers[0];

	for(i=0; i<NUMBER_OF_TIMEOUT_TIMERS; i++){

		if(timer->enabled == true){

			timer->counter++;

			if(timer->period <= timer->counter){
				timer->elapsed = true;
				timer->enabled = false;
			}
		}

		timer++;
	}
}


/* Private functions definitions ---------------------------------------------*/

#ifdef SYSTEM_CLOCK_8MHZ
/**
  * @brief   Configures hardware timer to generate interrupts every 1ms.
  * @param   None
  * @retval  None
  */
static void configureHardwareTimer(void){

	//The rest of configuration is in HAL_TIM_Base_MspInit().

	TIM_ClockConfigTypeDef sClockSourceConfig;
	TIM_MasterConfigTypeDef sMasterConfig;

	// Module update event period 1[ms]= (prescaler + 1) * period * APB1_CLK_period = (4 + 1) * 1600 * (1/(1*8Mhz))
	// therefore:
	// TIM period = Module_update_event_period * (1 * APB1_CLK_period)/(prescaler + 1)
	// TIM period = 0.001s * (1 * 8MHz)/(4 + 1) = 1600

	htim3.Instance = TIM3;
	htim3.Init.Prescaler = 4;
	htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim3.Init.Period = 1600;
	htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	HAL_TIM_Base_Init(&htim3);

	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig);

	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig);
}
#endif


#ifdef SYSTEM_CLOCK_16MHZ
/**
  * @brief   Configures hardware timer to generate interrupts every 1ms.
  * @param   None
  * @retval  None
  */
static void configureHardwareTimer(void){

	//The rest of configuration is in HAL_TIM_Base_MspInit().

	TIM_ClockConfigTypeDef sClockSourceConfig;
	TIM_MasterConfigTypeDef sMasterConfig;

	// Module update event period 1[ms]= (prescaler + 1) * period * APB1_CLK_period = (4 + 1) * 3200 * (1/(1*16Mhz))
	// therefore:
	// TIM period = Module_update_event_period * (1 * APB1_CLK_period)/(prescaler + 1)
	// TIM period = 0.001s * (1 * 16MHz)/(4 + 1) = 3200

	htim3.Instance = TIM3;
	htim3.Init.Prescaler = 4;
	htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim3.Init.Period = 3200;
	htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	HAL_TIM_Base_Init(&htim3);

	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig);

	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig);
}
#endif


#ifdef SYSTEM_CLOCK_80MHZ
/**
  * @brief   Configures hardware timer to generate interrupts every 1ms.
  * @param   None
  * @retval  None
  */
static void configureHardwareTimer(void){

	//The rest of configuration is in HAL_TIM_Base_MspInit().

	TIM_ClockConfigTypeDef sClockSourceConfig;
	TIM_MasterConfigTypeDef sMasterConfig;

	// Module update event period 1[ms]= (prescaler + 1) * period * APB1_CLK_period = (4 + 1) * 16000 * (1/(1*80Mhz))
	// therefore:
	// TIM period = Module_update_event_period * (1 * APB1_CLK_period)/(prescaler + 1)
	// TIM period = 0.001s * (1 * 80MHz)/(4 + 1) = 16000

	htim3.Instance = TIM3;
	htim3.Init.Prescaler = 4;
	htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim3.Init.Period = 16000;
	htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	HAL_TIM_Base_Init(&htim3);

	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig);

	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig);
}
#endif
