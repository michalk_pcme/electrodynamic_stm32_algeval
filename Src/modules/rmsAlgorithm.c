//**************************************************************************
//
// Date				06.12.2017
//
// Product			Electrodynamic sensor
//
// Or Module
//
// Copyright        PCME PROPRIETARY AND CONFIDENTIAL
//                  SOFTWARE FILE/MODULE HEADER
//                  PCME (c) 2016
//                  Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
//                  All Rights Reserved
//**************************************************************************


/* ----------------------- System includes --------------------------------*/
#include <math.h>

/* ----------------------- Application includes ---------------------------*/
#include "rmsAlgorithm.h"


/* ----------------------- Defines ----------------------------------------*/
/* ----------------------- Type Definitions -------------------------------*/
/* ----------------------- Local  variables -------------------------------*/
/* ----------------------- Local  Prototypes ------------------------------*/
static void rmsReSetWindow(rmsSqWindow_t* window);


/* ----------------------- Global Functions -------------------------------*/

/**
 * @brief This function should be called only once at the beginning
 * 		  of the life of the driver.
 */
void rmsPreSetDriver(rmsDriver_t* driver)
{
	if(0 == driver) return;
	
	driver->sqWindows = 0;
}


/**
 * @brief This function should be called at the end of the life of
 * 		  the driver or just to reset it for new configuration.
 */
void rmsReSetDriver(rmsDriver_t* driver)
{
	if(0 == driver) return;
	
	//free the allocated memory
	if(0 != driver->sqWindows)
	{
		safeFree(driver->sqWindows);
		driver->sqWindows = 0;
	}
}


/**
 * @brief This function initialises driver accordingly to configuration
 * 	      structure.
 */
bool rmsConfigureDriver(rmsDriver_t* driver, rmsConfig_t* newConfig)
{
	uint32_t i;

	if(0 == driver) 	return false;
	if(0 == newConfig) 	return false;
	
	//assign memory for the driver's RMS windows
	driver->sqWindows = safeMalloc(newConfig->nbrOfWindows * sizeof(rmsSqWindow_t));
	if(0 == driver->sqWindows) return false;
	
	//copy configuration
	driver->config = *newConfig;
	
	//configure the rest of the driver
	driver->activeWindow 	= 0;
	driver->totalSampCtr 	= 0;
	driver->maxSampCtr 		= newConfig->nbrOfSamplesPerWindow * newConfig->nbrOfWindows;
	driver->squareSums 		= 0;
	driver->rmsValue 		= 0;
	
	for(i=0; i<newConfig->nbrOfWindows; i++)
		rmsReSetWindow(&driver->sqWindows[i]);

	return true;
}



/**
 * @brief This function adds a new sample to the RMS driver.
 */
void rmsAddNewSample(rmsDriver_t* driver, float sample)
{
	rmsSqWindow_t * window;
	uint32_t i;
	
	//retrieve window
	window = &driver->sqWindows[driver->activeWindow];
	
	//add new sample to window
	window->value += sample*sample;
	window->ctr++;
	driver->totalSampCtr++;
	
	//check if whole window has been collected
	if(window->ctr == driver->config.nbrOfSamplesPerWindow)
	{
		//change the active window to the next one in a circular buffer manner
		driver->activeWindow++;
		if(driver->activeWindow == driver->config.nbrOfWindows)
			driver->activeWindow = 0;
		
		rmsReSetWindow(&driver->sqWindows[driver->activeWindow]);

		//if all the required samples have been collected, then calculate RMS value
		if(driver->totalSampCtr == driver->maxSampCtr)
		{
			//calculate new RMS value from all the windows/samples
			driver->rmsValue = 0;
			for(i=0; i<driver->config.nbrOfWindows; i++)
			{
				driver->rmsValue += driver->sqWindows[i].value;
			}
			driver->rmsValue = driver->rmsValue/(float)driver->totalSampCtr;
			driver->rmsValue = (float)sqrt(driver->rmsValue);
			
			//new window has been collected, driver->set totalSampCtr to such a value,
			//that next RMS is going to get calculated at the end of the next window
			driver->totalSampCtr -= driver->config.nbrOfSamplesPerWindow;
		}
	}
}


/**
 * @brief This function adds a new sample to the RMS driver.
 */
void rmsAddNewSampleOptimised(rmsDriver_t* driver, float sample)
{
	rmsSqWindow_t * window;

	//retrieve window
	window = &driver->sqWindows[driver->activeWindow];

	//add new sample to window
	window->value += sample*sample;
	window->ctr++;
	driver->totalSampCtr++;

	//check if whole window has been collected
	if(window->ctr == driver->config.nbrOfSamplesPerWindow)
	{
		//append collected new sum of squares
		driver->squareSums += window->value;

		//change the active window to the next one in a circular buffer manner (point to the oldest one)
		driver->activeWindow++;
		if(driver->activeWindow == driver->config.nbrOfWindows)
			driver->activeWindow = 0;

		window = &driver->sqWindows[driver->activeWindow];

		//if all the required samples have been collected, then calculate RMS value
		if(driver->totalSampCtr == driver->maxSampCtr)
		{
			//calculate new RMS value from all the windows/samples
			driver->rmsValue = (float)sqrt(driver->squareSums/driver->totalSampCtr);

			//new window has been collected, driver->set totalSampCtr to such a value,
			//that next RMS is going to get calculated at the end of the next window
			driver->totalSampCtr -= driver->config.nbrOfSamplesPerWindow;

			//subtract the oldest sum to get rid of the irrelevant samples in RMS calculations
			driver->squareSums -= window->value;
		}

		//reset the oldest window to prepare for the new collection of data
		rmsReSetWindow(window);
	}
}


/**
 * @brief   This function adds whole matrix of sample to the RMS driver.
 * @warning samples[] has to have driver->config.nbrOfSamplesPerWindow elements.
 */
void rmsAddNewSamplesMatrix(rmsDriver_t* driver, const float* samples)
{
	rmsSqWindow_t * window;
	uint32_t i;
	
	//retrieve window
	window = &driver->sqWindows[driver->activeWindow];
	
	//calculate square sums of the window
	window->value = 0;
	for(i=0; i<driver->config.nbrOfSamplesPerWindow; i++)
	{
		//TODO this line is problematic if samples vary greatly in values (floats should be summed
		//from smallest to largest, otherwise small values might disappear at the resolution edge 
		//of a "big" sample)
		window->value += *samples * *samples;
		samples++;
	}
	
	//this line is irrelevant in this case
	window->ctr = driver->config.nbrOfSamplesPerWindow;
		
	//change the active window to the next one in a circular buffer manner
	driver->activeWindow++;
	if(driver->activeWindow == driver->config.nbrOfWindows)
		driver->activeWindow = 0;
	
	//update total number of collected samples
	driver->totalSampCtr += driver->config.nbrOfSamplesPerWindow;
	
	//if all the required samples have been collected, then calculate RMS value
	if(driver->totalSampCtr == driver->maxSampCtr)
	{
		//calculate new RMS value from all the windows/samples
		driver->rmsValue = 0;
		for(i=0; i<driver->config.nbrOfWindows; i++)
		{
			driver->rmsValue += driver->sqWindows[i].value;
		}
		driver->rmsValue = driver->rmsValue/(float)driver->totalSampCtr;
		driver->rmsValue = (float)sqrt(driver->rmsValue);
		
		//new window has been collected, therefore prepare space for samples from a new one
		driver->totalSampCtr -= driver->config.nbrOfSamplesPerWindow;
	}	
}


/**
 * @brief Returns latest RMS value.
 */
float rmsGetRMSValue(rmsDriver_t* driver)
{
	return driver->rmsValue;
}




void  fakeRmsPreSetDriver(fakeRmsDriver_t* driver)
{
	(void)(driver);
}


void  fakeRmsReSetDriver(fakeRmsDriver_t* driver)
{
	(void)(driver);
}


bool  fakeRmsConfigureDriver(fakeRmsDriver_t* driver, fakeRmsConfig_t* newConfig)
{
	if(0 == driver) 	return false;
	if(0 == newConfig) 	return false;

	//copy configuration
	driver->config = *newConfig;

	//configure the rest of the driver
	driver->rmsValue = 0;

	raResetDriver(&driver->rollAvg, newConfig->nbrOfSamples);

	return true;
}


void  fakeRmsAddNewSample(fakeRmsDriver_t* driver, float sample)
{
	float temp;

	//add new sample
	raAppendNewValue(&driver->rollAvg, sample*sample);

	//calculate RMS
	temp = raGetAvgValue(&driver->rollAvg);
	driver->rmsValue = (float)sqrt(temp);
}


float fakeRmsGetRMSValue(fakeRmsDriver_t* driver)
{
	return driver->rmsValue;
}



/* ----------------------- Local  Functions -------------------------------*/

static void rmsReSetWindow(rmsSqWindow_t* window)
{
	if(0 == window) return;

	window->ctr 	= 0;
	window->value 	= 0;
}













