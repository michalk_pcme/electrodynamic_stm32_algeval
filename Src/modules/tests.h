/*
 * tests.h
 *
 *  Created on: 11 Dec 2017
 *      Author: michalk
 */

#ifndef MODULES_TESTS_H_
#define MODULES_TESTS_H_



void testRMS_1(void);
void testRMS_2(void);
void testRMS_3(void);

void testFIR_1(void);
void testFIR_2(void);

void startWrsmUartTest(void);
void startWrsmOptimisedUartTest(void);
void startFakeWrsmUartTest(void);
void startFirUartTest(void);


#endif /* MODULES_TESTS_H_ */
