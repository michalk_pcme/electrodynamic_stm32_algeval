//**************************************************************************
//
// Date             15.09.2016
//
// Product          
//
// Or Module        Simple serial protocol.
//
// Copyright        PCME PROPRIETARY AND CONFIDENTIAL
//                  SOFTWARE FILE/MODULE HEADER
//                  PCME (c) 2016
//                  Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
//                  All Rights Reserved
//**************************************************************************


/* ----------------------- System includes --------------------------------*/
#include "systemDefinitions.h"


/* ----------------------- Application includes ---------------------------*/
#include "acEngine.h"
#include "acCommands.h"
#include "timeoutTimers.h"


/* ----------------------- Defines ----------------------------------------*/
/* ----------------------- Type Definitions -------------------------------*/
/* ----------------------- Local  variables -------------------------------*/
static commRxController_t rxCntr;
static commTxController_t txCntr;

static templatePar_t templateVal;


/* ----------------------- Local  Prototypes ------------------------------*/
static void sendCommandUnknownMsg(void);
static void sendTransmissionErrorMsg(void);

void checkRxTimeout(void);

static bool addToRxBuf(uint8_t RX);
static void binComAnalyse(void);
static bool binCommRxProtocol(uint8_t RX);

static void resetActiveRxConnection(void);
static void resetRxController(void);
static void resetTxController(void);
static void resetControllers(void);


/* ----------------------- Global Functions -------------------------------*/
void acInitAuxComms(void){
    
#ifdef AC_RUNNING_ON_RTOS
    //initialise global storage task
    xTaskCreate((TaskFunction_t) acTask,
                "Glob stor tsk",
                AC_TASK_SIZE, NULL, AUX_COMMS_TASK_PRIORITY, NULL);
#endif
    
	serialPortHardwareInit();
    resetControllers();
    resetTimeoutTimer(TT_AUX_USART_WAIT_FOR_NEW_CHAR);

    EN_SERIAL_RX_INT
}



void acTask(void)
{
#ifdef AC_RUNNING_ON_RTOS
    while(1)
    {
        acProcess();
        
        //wait a while
        vTaskDelay(SERVICES_REFRESH_PERIOD/portTICK_PERIOD_MS);
    }
#endif
}


void acProcess(void)
{    
    //check if there was is any ongoing broken RX communication
    checkRxTimeout();

    //check if there are any successfully received messages that need processing
	if (rxCntr.analysed_commands_ctr < rxCntr.commands_ctr){

		binComAnalyse();		
		rxCntr.analysed_commands_ctr++;
		
        //if all the messages are being analysed and there is no receiving
        //communication going on, then reset RX structures.
        AC_DINT;
		if ((rxCntr.analysed_commands_ctr == rxCntr.commands_ctr) && (rxCntr.active == false)) {
			resetRxController();
		}
        AC_EINT;
	}
}


bool addShortMessageToQueue(commandsIds cmdId){
    TxQueueItem_t msg;
    
    msg.command   = cmdId;
    msg.paramPtr  = 0;
    msg.paramSize = 0;
    
    return addMessageToQueue(&msg);
}


bool addMessageToQueue(TxQueueItem_t * Item)
{
    //check if there is space to add new item
	if(txCntr.queueStc.w_ctr >= AC_TX_BUF_SIZE)
        return false;
        
	//exclusive access to queue counters has to be granted!!!
	//global interrupts have to be disabled (or at least the ones that use add_message_to_queue, like PWM1 and TxIsr)
	AC_DINT;
	
	txCntr.status = SENDING_QUEUE;
	txCntr.queueStc.queue[txCntr.queueStc.w_ctr] = *Item;
	txCntr.queueStc.w_ctr++;
	EN_SERIAL_TX_INT
	
    AC_EINT;
	return true;
}


/**
 * @warning This function is going to block until the whole message gets sent.
 * 			Incoming communication is not going to get processed while blocking.
 */
bool addMessageToQueueBlockingMode(TxQueueItem_t * Item)
{
	//add message to the TX queue
	if(false == addMessageToQueue(Item))
		return false;

	//wait for the message to get sent
	while(false == isTxMessageQueueEmpty()){}

	return true;
}


bool isTxMessageQueueEmpty(void)
{
	bool result = false;

	if(0 == txCntr.queueStc.w_ctr)
		result = true;

	return result;
}


void serialRxInterrupt(uint8_t RX, bool errorFlag)
{    
    //if there was an error detected in communication then reset what 
    //has been already going on
    if(true == errorFlag) goto err;
    
    //process reception protocol
    if(false == rxCntr.active){
		if('B' == RX){
            //identifying byte does match
            //initialise structures for reception of a new message
            
            //make reception active
            rxCntr.active = true;
            
            //reset binary protocol structure
            rxCntr.binary_rx.status = GET_LENGTH;
			rxCntr.binary_rx.acCRC = 0;
			rxCntr.binary_rx.acCRC ^= RX;
            
            //prepare reception timeout timer
            setTimeoutTimer(TT_AUX_USART_WAIT_FOR_NEW_CHAR, UART_WAIT_FOR_NEW_CHAR_TIMEOUT);

            //prepare buffer for reception of new command
            //TODO clean up after this line if not needed
            //rxCntr.buffer.starting_point = rxCntr.buffer.w_ctr;
            
            //add new byte to the buffer
            if(false == addToRxBuf(RX)) goto err;
        }
    }
    else
        //communication has been already going on, therefore continue
        if(false == binCommRxProtocol(RX)) goto err;
    
    return;
    
err:
    resetActiveRxConnection();
}


void serialTxInterrupt(void)
{	
	volatile TxQueueItem_t* Item;
	uint8_t tempVal;

	if(SENDING_QUEUE != txCntr.status) return;
			
	//for simplicity of code pointer to queue item is used
	Item = &txCntr.queueStc.queue[txCntr.queueStc.r_ctr];

	switch(txCntr.FrameState){
    case FS_NONE:
        //prepare structure for transmission
        txCntr.paramPtr  = Item->paramPtr;
        txCntr.paramCtr = 0;
        txCntr.acCRC = 0;
        txCntr.FrameState = FS_PROTOCOL_NAME;

        //No break condition over here, just go over to next step.
        //break;
					
        
	case FS_PROTOCOL_NAME:
		SERIAL_TX_BUF = 'B';
		txCntr.acCRC ^= 'B';
		txCntr.FrameState = FS_LENGTH;
		break;

        
	case FS_LENGTH:
        // lower and higher halfword(length of parameters * 2) + command name 
        tempVal = Item->paramSize + 1;
        SERIAL_TX_BUF = tempVal;
		txCntr.acCRC    ^= tempVal;
		txCntr.FrameState = FS_COMMAND;
	break;
	
    
	case FS_COMMAND:
		SERIAL_TX_BUF = (uint8_t)Item->command;
		txCntr.acCRC    ^= (uint8_t)Item->command;
           
        if(0 != Item->paramSize)
            txCntr.FrameState = FS_DATA;
        else 
            txCntr.FrameState = FS_CRC;
            
        break;
				
        
	case FS_DATA:
        //it's still before ending address with data to send, so send data through
        SERIAL_TX_BUF = *txCntr.paramPtr;
        txCntr.acCRC    ^= *txCntr.paramPtr;
        txCntr.paramPtr++;
        txCntr.paramCtr++;
            
        //check if all parameters got sent
        if(txCntr.paramCtr == Item->paramSize)
			txCntr.FrameState = FS_CRC;
		break;

        
	case FS_CRC:
		SERIAL_TX_BUF = txCntr.acCRC;
		
        //check if there is anything else queued for sending
        if(txCntr.queueStc.r_ctr == (txCntr.queueStc.w_ctr-1)){
            //everything got sent, clear transmitting structures
            resetTxController();
        }
        else{
            //there is still something to be sent in queue
            txCntr.queueStc.r_ctr++;
            txCntr.FrameState = FS_NONE;
        }
		break;
	}
}


void sendTemplateMsg(void)
{
    templateVal.valA = 0xDEADBEEF;
    templateVal.valB = 0xAB;
    templateVal.valC = 10.5;
    
    TxQueueItem_t item;
    item.paramPtr  = (unsigned char*)&templateVal;
    item.paramSize = sizeof(templatePar_t);
    item.command   = COMM_TEMPLATE;
        
    addMessageToQueue(&item);    
}


/* ----------------------- Local  Functions -------------------------------*/
static void sendCommandUnknownMsg(void)
{
	TxQueueItem_t Item;

	Item.command  = COMM_UNKNOWN_COMMAND;
	Item.paramPtr = 0;
	Item.paramSize = 0;

	addMessageToQueue(&Item);
}


static void sendTransmissionErrorMsg(void)
{
	TxQueueItem_t Item;

	Item.command  = COMM_TRANSMISSION_ERROR;
	Item.paramPtr = 0;
	Item.paramSize = 0;

	addMessageToQueue(&Item);
}


void checkRxTimeout(void)
{
    if(false == hasTimeoutTimerElapsed(TT_AUX_USART_WAIT_FOR_NEW_CHAR)) return;
    
    //timer has elapsed, which means broken communication, restore RX communication stack
    AC_DINT;
    resetActiveRxConnection();
    AC_EINT;
}


static bool addToRxBuf(uint8_t RX){
    
    //check if there is space for new byte
	if(rxCntr.buffer.w_ctr >= AC_RX_BUF_SIZE) return false;
    
    //add new byte to buffer
	rxCntr.buffer.data[rxCntr.buffer.w_ctr] = RX;
	rxCntr.buffer.w_ctr++;
    
    return true;
}


static void binComAnalyse(void){

	uint8_t i;
    
    uint8_t messageLength =  rxCntr.buffer.data[rxCntr.buffer.r_ctr+1];
    uint8_t commandId     =  rxCntr.buffer.data[rxCntr.buffer.r_ctr+2];
	uint8_t* param_ptr    = &rxCntr.buffer.data[rxCntr.buffer.r_ctr+3];
	
    bool command_executed = false;
    
	for (i=0;i<COMMANDS_AMOUNT;i++){
	
		if (USART_Commands[i].bin_alias == commandId){
			
			//found proper command
			USART_Commands[i].function_ptr(param_ptr);	//executing proper command function
			command_executed = true;
			
			//the end of searching and getting out of loops
			i=COMMANDS_AMOUNT;
		}
	}

	if (command_executed == false)
		sendCommandUnknownMsg();
        	
	//setting reading "pointer" to the beginning of next message in the buffer
	//                    current start       + length of binary message + length byte + starting byte
	rxCntr.buffer.r_ctr = rxCntr.buffer.r_ctr + messageLength + 1 + 1;
}


static bool binCommRxProtocol(uint8_t RX){
	
	setTimeoutTimer(TT_AUX_USART_WAIT_FOR_NEW_CHAR, UART_WAIT_FOR_NEW_CHAR_TIMEOUT);

	switch (rxCntr.binary_rx.status)
	{
	case GET_LENGTH: 
        rxCntr.binary_rx.length_ctr = RX;
        rxCntr.binary_rx.acCRC ^= RX;
		rxCntr.binary_rx.status = RECEIVE_DATA;
        if(false == addToRxBuf(RX)) return false;
		break;
	
	case RECEIVE_DATA:
		rxCntr.binary_rx.acCRC ^= RX;
		rxCntr.binary_rx.length_ctr--;
        if(false == addToRxBuf(RX)) return false;

        if (rxCntr.binary_rx.length_ctr == 0){
			rxCntr.binary_rx.status = CHECK_CRC;
		}
		break;

	case CHECK_CRC:
#ifdef IGNORE_CRC
		ConnStc.binary_rx.acCRC = RX;
#endif
        //check if CRC matches
        if (rxCntr.binary_rx.acCRC != RX) return false;
        
        //message received successfully
        rxCntr.commands_ctr++;
		rxCntr.active = false;
		resetTimeoutTimer(TT_AUX_USART_WAIT_FOR_NEW_CHAR);
		rxCntr.buffer.starting_point = rxCntr.buffer.w_ctr; //because it may hapen a reset connection while not receiving anything
		break;
	}

    return true;	
}


static void resetActiveRxConnection(void){
	rxCntr.active = false;
    rxCntr.buffer.w_ctr = rxCntr.buffer.starting_point;
    resetTimeoutTimer(TT_AUX_USART_WAIT_FOR_NEW_CHAR);
	sendTransmissionErrorMsg();
}


static void resetRxController(void){

	rxCntr.active = false;
	rxCntr.commands_ctr = 0;
	rxCntr.analysed_commands_ctr = 0;

	rxCntr.buffer.w_ctr = 0;
	rxCntr.buffer.starting_point = 0;
	rxCntr.buffer.r_ctr = 0;
	
	rxCntr.binary_rx.length_ctr = 0;
}


static void resetTxController(void)
{
    txCntr.status = READY;
	txCntr.queueStc.r_ctr = 0;
	txCntr.queueStc.w_ctr = 0;
	txCntr.FrameState = FS_NONE;
    DIS_SERIAL_TX_INT
}


static void resetControllers(void)
{
    resetRxController();
	resetTxController();
}

