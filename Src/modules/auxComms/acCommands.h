//**************************************************************************
//
// Date             15.09.2016
//
// Product          
//
// Or Module        Simple serial protocol.
//
// Copyright        PCME PROPRIETARY AND CONFIDENTIAL
//                  SOFTWARE FILE/MODULE HEADER
//                  PCME (c) 2016
//                  Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
//                  All Rights Reserved
//**************************************************************************


#ifndef _AC_COMMANDS_H_
#define _AC_COMMANDS_H_


/* ----------------------- System includes --------------------------------*/
#include "systemDefinitions.h"


/* ----------------------- Application includes ---------------------------*/
#include "acPort.h"


/* ----------------------- Defines ----------------------------------------*/
/**Defines number of recognised commands*/
#define COMMANDS_AMOUNT 3


/* ----------------------- Type Definitions -------------------------------*/
typedef enum{
    //protocol specific IDs from 0 to 9 are reserved
    //DO NOT USE ANY OF THOSE COMMAND IDS in code
    COMM_NOTHING 					= 0,

    COMM_TRANSMISSION_ERROR  		= 1,
    COMM_UNKNOWN_COMMAND 	 		= 2,

    //USER'S COMMAND ID SPACE
    //those IDs can be used in a code
    COMM_TEMPLATE 					= 10,
    
	//RMS algorithm check
	COMM_START_WRMS_CALCULATIONS	= 11,
	COMM_WRMS_DATA					= 12,
	COMM_FINISHED_WRMS_CALCULATIONS	= 13,

	//FIR algorithm check
	COMM_START_FIR_CALCULATIONS		= 21,
	COMM_FIR_DATA					= 22,
	COMM_FINISHED_FIR_CALCULATIONS	= 23
}commandsIds;


typedef struct
{
    uint32_t  valA;
    uint8_t   valB;
    float     valC;
}__attribute__ ((packed)) templatePar_t;


typedef struct
{
	float data;
}__attribute__ ((packed)) wrmsDataParams_t;


typedef struct
{
	float data;
}__attribute__ ((packed)) firDataParams_t;



/** @brief	Specifies command structure with its related function. */
typedef struct
{
	commandsIds bin_alias;
	void (*function_ptr)(uint8_t*);
}Command_Stc;


/* ----------------------- Global variables -------------------------------*/
extern Command_Stc USART_Commands[COMMANDS_AMOUNT];


/* ----------------------- Global Prototypes ------------------------------*/


#endif /* _AC_COMMANDS_H_ */
