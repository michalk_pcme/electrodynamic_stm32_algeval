//**************************************************************************
//
// Date             15.09.2016
//
// Product          
//
// Or Module        Simple serial protocol.
//
// Copyright        PCME PROPRIETARY AND CONFIDENTIAL
//                  SOFTWARE FILE/MODULE HEADER
//                  PCME (c) 2016
//                  Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
//                  All Rights Reserved
//**************************************************************************


#ifndef _AC_PORT_H_
#define _AC_PORT_H_


/* ----------------------- System includes --------------------------------*/
#include "systemDefinitions.h"
#include "stm32l4xx_hal.h"


/* ----------------------- Application includes ---------------------------*/
#include "timeoutTimers.h"


/* ----------------------- Defines ----------------------------------------*/
//#define AC_RUNNING_ON_RTOS

//#define AC_PIC32_HARDWARE
#define AC_STM32_HARDWARE


#ifdef AC_PIC32_HARDWARE
#define DIS_SERIAL_TX_INT   IEC4bits.U2TXIE = 0;
#define EN_SERIAL_TX_INT    IEC4bits.U2TXIE = 1;

#define DIS_SERIAL_RX_INT   IEC4bits.U2RXIE = 0;
#define EN_SERIAL_RX_INT    IEC4bits.U2RXIE = 1;

#define SERIAL_TX_BUF       U2TXREG
#define SERIAL_RX_BUF       U2RXREG
#endif


#ifdef AC_STM32_HARDWARE
#define DIS_SERIAL_TX_INT   USART2->CR1 &= ~USART_CR1_TXEIE;
#define EN_SERIAL_TX_INT    USART2->CR1 |= USART_CR1_TXEIE;

#define DIS_SERIAL_RX_INT   USART2->CR1 &= ~USART_CR1_RXNEIE;
#define EN_SERIAL_RX_INT    USART2->CR1 |= USART_CR1_RXNEIE;

#define SERIAL_TX_BUF       USART2->TDR
#define SERIAL_RX_BUF       USART2->RDR
#endif


#define AC_DINT				DIS_INT
#define AC_EINT				EN_INT


/* ----------------------- Type Definitions -------------------------------*/
/* ----------------------- Global variables -------------------------------*/
/* ----------------------- Global Prototypes ------------------------------*/
void serialPortHardwareInit(void);

#ifdef AC_PIC32_HARDWARE
void acUartTxInterruptHandler(void);
void acUartRxInterruptHandler(void);
#endif

#ifdef AC_STM32_HARDWARE
void acUartInterruptHandler(void);
#endif

#endif /* _AC_PORT_H_ */
