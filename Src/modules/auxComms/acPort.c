//**************************************************************************
//
// Date             15.09.2016
//
// Product          
//
// Or Module        Simple serial protocol.
//
// Copyright        PCME PROPRIETARY AND CONFIDENTIAL
//                  SOFTWARE FILE/MODULE HEADER
//                  PCME (c) 2016
//                  Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
//                  All Rights Reserved
//**************************************************************************


/* ----------------------- System includes --------------------------------*/
#include "systemDefinitions.h"
#include "stm32l4xx_hal.h"
#include "usart.h"


/* ----------------------- Application includes ---------------------------*/
#include "acPort.h"
#include "acEngine.h"


/* ----------------------- Type Definitions -------------------------------*/
/* ----------------------- Local  variables -------------------------------*/
/* ----------------------- Local  Prototypes ------------------------------*/
/* ----------------------- Global Functions -------------------------------*/

#ifdef AC_PIC32_HARDWARE
/**
  * @brief  Initialises serial port peripheral registers for USART 2.
  * @note	115200 BAUD/s, no parity, 8 data bits, 1 stop bit, no handshake, no FIFO
  */
void serialPortHardwareInit(void)
{
    //-------- USART2 GPIO's INITIALISATION
    //Those pins are only digital pins, therefore no ANSELx is available for them
        
    RPG9R = 2;  //map UART2 TX onto RDG9 pin   
    U2RXR = 0;  //map UART2 RX onto RPD9 pin
        
    //-------- USART2 PERIPHERAL INITIALISATION AS RX/TX

    //U4BRG = 321;    //19200 BAUD
    //U4BRG = 160;    //38400 BAUD
    //U4BRG = 106;    //57600 BAUD
    U2BRG = 53;     //115200 BAUD

    U2STAbits.UTXEN = 1;    //enable transmitter
    U2STAbits.URXEN = 1;    //enable receiver
    U2STAbits.OERR = 0;     //clear Receive Buffer Overrun Error Status bit.

    U2MODEbits.BRGH = 0;    //Standard Speed mode ? 16x baud clock enabled
    U2MODEbits.ON = 1;      //enable UART


    //-------- USART2 interrupt initialisation

    //interrupt is generated when all characters have been transmitted
    U2STASET = 0X00004000; //U4STAbits.UTXSEL( bit 15,14) =01; 

    //USART2 TX interrupt
    IPC36bits.U2TXIS = 0;                       //sub-priority
    IPC36bits.U2TXIP = UART2_TX_ISR_PRIORITY;   //priority
    IFS4bits.U2TXIF = 0;                        //interrupt flag reset
    IEC4bits.U2TXIE = 0;                        //interrupt disable
    
    //USART2 RX interrupt
    IPC36bits.U2RXIS = 0;
    IPC36bits.U2RXIP = UART2_RX_ISR_PRIORITY;
    IFS4bits.U2RXIF = 0;
    IEC4bits.U2RXIE = 1;
    
    //USART2 FAULT interrupt
    IPC36bits.U2EIS = 1;
    IPC36bits.U2EIP = 1;
    IFS4bits.U2EIF = 0;
    IEC4bits.U2EIE = 0;
}


void acUartTxInterruptHandler(void)
{
    serialTxInterrupt();
    
    IFS4bits.U2TXIF = 0;
}


void acUartRxInterruptHandler(void)
{
    volatile unsigned char rx;
    volatile unsigned int staReg;
    volatile bool error = false;
    
    //get new rx character
    rx = U2RXREG;
    
    //check for errors
    staReg = U2STA;
    if(0 != (staReg & (unsigned int)0x000E)){
        
        //set the error flag
        error = true;
        
        //clear errors
        U2STAbits.OERR = 0;
    }
    
    //add new character to the serial engine
    serialRxInterrupt(rx, error);
    
    //clear rx interrupt flag
    IFS4bits.U2RXIF = 0;
}
#endif



#ifdef AC_STM32_HARDWARE
void serialPortHardwareInit(void)
{
	//MX_USART1_UART_Init();
	MX_USART2_UART_Init();
}


#if 0
void acUartInterruptHandler(void)
{
	volatile uint16_t data;
	volatile uint32_t reg;
	bool validData = true;

	reg = (uint16_t)0x3FF & USART1->ISR;
	data = (uint16_t)0xFF & SERIAL_RX_BUF;

	//check if interrupt caused by reception of new character
	if(reg & UART_FLAG_RXNE){
		if(0 != (reg & (USART_FLAG_PE | USART_FLAG_FE | USART_FLAG_ORE | USART_FLAG_NE /*| USART_FLAG_IDLE*/ ))){
			validData = false;
			//clearing flags not needed as they should be already cleared automatically earlier on
			//by reading SR and DR registers in this order
			//USART1->ISR &= 0;
		}
		serialRxInterrupt(data, validData);
	}

	//check if interrupt caused by finished transmission of character
	if(reg & UART_FLAG_TXE){
		serialTxInterrupt();
	}

	//clear some additional eventual flags if they pop up somehow
	USART1->ISR &= ~(USART_FLAG_CTS|USART_FLAG_LBDF);
}
#endif


void acUartInterruptHandler(void)
{
	volatile uint16_t data;
	volatile uint32_t reg;
	bool errorFlag = false;

	reg = (uint16_t)0x3FF & USART2->ISR;
	data = (uint16_t)0xFF & SERIAL_RX_BUF;

	//check if interrupt caused by reception of new character
	if(reg & UART_FLAG_RXNE){
		if(0 != (reg & (USART_FLAG_PE | USART_FLAG_FE | USART_FLAG_ORE | USART_FLAG_NE /*| USART_FLAG_IDLE*/ ))){
			errorFlag = true;
			//clearing flags not needed as they should be already cleared automatically earlier on
			//by reading SR and DR registers in this order
			//USART2->ISR &= 0;
		}
		serialRxInterrupt(data, errorFlag);
	}

	//check if interrupt caused by finished transmission of character
	if(reg & UART_FLAG_TXE){
		serialTxInterrupt();
	}

	//clear some additional eventual flags if they pop up somehow
	USART2->ISR &= ~(USART_FLAG_CTS|USART_FLAG_LBDF);
}


#endif

/* ----------------------- Local  Functions -------------------------------*/


