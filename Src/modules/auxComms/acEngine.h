//**************************************************************************
//
// Date             15.09.2016
//
// Product          
//
// Or Module        Simple serial protocol.
//
// Copyright        PCME PROPRIETARY AND CONFIDENTIAL
//                  SOFTWARE FILE/MODULE HEADER
//                  PCME (c) 2016
//                  Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
//                  All Rights Reserved
//**************************************************************************


#ifndef _AC_ENGINE_H_
#define _AC_ENGINE_H_


/* ----------------------- System includes --------------------------------*/
#include "systemDefinitions.h"


/* ----------------------- Application includes ---------------------------*/
#include "acPort.h"
#include "acCommands.h"


/* ----------------------- Defines ----------------------------------------*/
#define AC_TASK_SIZE    512

/*Do not perform CRC comparison at the end of message processing*/
//#define IGNORE_CRC

/**@brief numbers of characters in RX buffer*/
#define AC_RX_BUF_SIZE 128

/**@brief Numbers of slots/structures in TX queue*/
#define AC_TX_BUF_SIZE 8


/* ----------------------- Type Definitions -------------------------------*/
typedef enum
{
    GET_LENGTH,
    CHECK_CRC,
    RECEIVE_DATA
}rxStates_t;


typedef enum 
{   READY,
    SENDING_QUEUE
}txStates_t;


typedef enum 
{
    FS_NONE,
    FS_PROTOCOL_NAME,
    FS_LENGTH,
    FS_COMMAND,
    FS_DATA,
    FS_CRC
}frameStates_t;


/**@brief Specifies message item for transmission. */
typedef struct
{
	uint8_t* paramPtr;				/*!< Points to beggining of the memory that should be sent as parameter*/
	uint8_t  paramSize;				/*!< Size of parameters in bytes*/
	commandsIds command;		/*!< Specifies command for binary transmission that should be
									 embedded into message*/
} TxQueueItem_t;


/**@brief Specifies RX controller driver. */
typedef struct
{
	bool active;					/*!< Specifies transmission/connection status*/
	
	struct{
		uint8_t data[AC_RX_BUF_SIZE];	/*!< Specifies buffer for received bytes*/
		uint16_t starting_point;		/*!< Specifies starting point for actually receiving command*/
		uint16_t w_ctr;					/*!< Specifies counter/pointer to place in buffor for next received character*/
		uint16_t r_ctr;					/*!< Specifies counter/pointer to place where next command analyse should start*/
	}buffer;

	struct{
		uint16_t length_ctr;			/*!< Specifies binary message length counter*/
		uint16_t CRC_ctr;				/*!< @warning NOT IN USE!*/
		uint16_t acCRC;					/*!< Specifies XOR checksum from whole binary message*/
		rxStates_t status;              /*!< Specifies actual state of binary transmission*/
	}binary_rx;

	uint16_t commands_ctr;				/*!< Specifies commands counter in the buffor*/
	uint16_t analysed_commands_ctr;		/*!< Specifies commands counter that were analysed*/

}commRxController_t;


/**@brief Specifies TX controller driver. */
typedef struct
{
	txStates_t    status;					/*!< Specifies state of the sending structure*/
	frameStates_t FrameState;				/*!< Specifies state of the protocol of the currently sent message
                                                 (only in binary mode)*/

	uint8_t* paramPtr;						/*!< Points to the memory that is currently being sent as message parameter*/
	uint8_t  paramCtr;						/*!< Counts number of sent parameter bytes*/
	uint8_t  acCRC;							/*!< Calculated CRC value of the current message*/
	
	struct{
		TxQueueItem_t queue[AC_TX_BUF_SIZE];/*!< Specifies sending queue matrix*/
		uint16_t w_ctr;						/*!< Points to empty slot in queue where new message can be added*/
		uint16_t r_ctr;						/*!< Points to currently being sent item in queue*/
	}queueStc;
}commTxController_t;


/* ----------------------- Global variables -------------------------------*/
/* ----------------------- Global Prototypes ------------------------------*/
void acInitAuxComms(void);
void acProcess(void);

bool addShortMessageToQueue(commandsIds cmdId);
bool addMessageToQueue(TxQueueItem_t * Item);
bool addMessageToQueueBlockingMode(TxQueueItem_t * Item);
bool isTxMessageQueueEmpty(void);

void serialRxInterrupt(uint8_t RX, bool errorFlag);     //INTERRUPT FUNCTION
void serialTxInterrupt(void);                           //INTERRUPT FUNCTION

void sendTemplateMsg(void);

void acTask(void);

#endif /* _AC_ENGINE_H_ */
