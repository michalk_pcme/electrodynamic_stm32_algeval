//**************************************************************************
//
// Date             15.09.2016
//
// Product          
//
// Or Module        Simple serial protocol.
//
// Note             This file does not strictly follow C template format.
//
// Copyright        PCME PROPRIETARY AND CONFIDENTIAL
//                  SOFTWARE FILE/MODULE HEADER
//                  PCME (c) 2016
//                  Clearview Bldg, Edison Rd, Saint Ives PE27 3GH
//                  All Rights Reserved
//**************************************************************************


/* ----------------------- System includes --------------------------------*/
#include "systemDefinitions.h"


/* ----------------------- Application includes ---------------------------*/
#include "acCommands.h"
#include "acEngine.h"
#include "acPort.h"

#include "tests.h"


/* ----------------------- Defines ----------------------------------------*/
/* ----------------------- Type Definitions -------------------------------*/
/* ----------------------- Local  Prototypes ------------------------------*/
static void templateCommand(uint8_t* paramPtr);
static void startWrmsCalculations(uint8_t* paramPtr);
static void startFirCalculations(uint8_t* paramPtr);


/* ----------------------- Global  variables ------------------------------*/

/**
 * @brief	matrix that stores commands numbers for binary protocol,
 *  		names for text protocol and commands function pointers.
 * 
 * @warning REMEMBER TO CHANGE COMMANDS_AMOUNT TO MATCH NUMBER OF
 *          COMMANDS IN MATRIX!
 */
Command_Stc USART_Commands[COMMANDS_AMOUNT] = {  
	{COMM_TEMPLATE,                     &templateCommand},
	{COMM_START_WRMS_CALCULATIONS,      &startWrmsCalculations},
	{COMM_START_FIR_CALCULATIONS,       &startFirCalculations}
};


/* ----------------------- Local  variables -------------------------------*/



/* ----------------------- Global Functions -------------------------------*/
/* ----------------------- Local  Functions -------------------------------*/

/**
  * @brief	Template command.
  * @param  paramPtr: 	 pointer to command parameters in serial communication
  * 					 receive buffer.
  */
static void templateCommand(uint8_t* paramPtr)
{
    volatile templatePar_t * temp = (templatePar_t*)paramPtr;
    
    //suppress compiler's warning
    (void)(temp);
    
    //generate own template message as a reply
	sendTemplateMsg();
}


static void startWrmsCalculations(uint8_t* paramPtr)
{
	(void)(paramPtr);

	//startWrsmUartTest();
	startWrsmOptimisedUartTest();
	//startFakeWrsmUartTest();
}


static void startFirCalculations(uint8_t* paramPtr)
{
	(void)(paramPtr);

	startFirUartTest();
}


