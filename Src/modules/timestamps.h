/**
  ******************************************************************************
  * @file	 timestamp.h
  * @author  Michal Kalbarczyk
  * @version
  * @date	 18 Jun 2015
  * @brief
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef TIMESTAMPS_H_
#define TIMESTAMPS_H_


/* Includes ------------------------------------------------------------------*/
#include <systemDefinitions.h>
#include "stm32l4xx_hal.h"


/* Exported constants --------------------------------------------------------*/
/* Exported define -----------------------------------------------------------*/
#define STAMPS_ENABLE
//#define NAMES_ONLY
#define DELTA
//#define USE_FILTER

#define TIME_STAMPS_AMOUNT 100

#define FILTER_LOW		10
#define FILTER_HIGH		11


/* Exported types ------------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/
void startCollectingTimestamps(unsigned int);
void collectTimestamp(unsigned int);
void initTimestamps(void);


/* Exported interrupt functions ----------------------------------------------*/
/* Exported variables --------------------------------------------------------*/
extern TIM_HandleTypeDef htim2;


#endif /* TIMESTAMPS_H_ */
