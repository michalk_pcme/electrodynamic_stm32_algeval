/**
  ******************************************************************************
  * @file	 private_defines.h
  * @author  Michal Kalbarczyk
  * @version
  * @date	 18 May 2015
  * @brief
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef PRIVATEDEFINES_H_
#define PRIVATEDEFINES_H_


/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"


/* Exported constants --------------------------------------------------------*/
/**
 * @brief Enables interrupt disable/enable management through raise of a BASEPRI
 * 		  level otherwise all the interrupts get disabled.
 */
//#define INT_MANAGE_BASEPRI_RAISE

#define SYSTEM_CLOCK_8MHZ
//#define SYSTEM_CLOCK_16MHZ
//#define SYSTEM_CLOCK_80MHZ


#ifdef SYSTEM_CLOCK_8MHZ
#define SYS_CLK					8000000UL
#define APB1_CLK				SYS_CLK
#define APB2_CLK				SYS_CLK
#endif

#ifdef SYSTEM_CLOCK_16MHZ
#define SYS_CLK					16000000UL
#define APB1_CLK				SYS_CLK
#define APB2_CLK				SYS_CLK
#endif

#ifdef SYSTEM_CLOCK_80MHZ
#define SYS_CLK					80000000UL
#define APB1_CLK				SYS_CLK
#define APB2_CLK				SYS_CLK
#endif




/* Exported types ------------------------------------------------------------*/
typedef enum {true = 1, false = 0} bool;


/* Exported macro ------------------------------------------------------------*/
#define DIS_INT disableInterrupts();
#define EN_INT  enableInterrupts();


/* Exported functions --------------------------------------------------------*/
void disableInterrupts(void);
void enableInterrupts(void);
void safeFree(void*);
void * safeMalloc(uint32_t);

//extern inline void __SVC(void);


/* Exported interrupt functions ----------------------------------------------*/
/* Exported variables --------------------------------------------------------*/



#endif /* PRIVATEDEFINES_H_ */
